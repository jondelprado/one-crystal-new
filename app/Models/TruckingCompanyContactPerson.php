<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TruckingCompanyContactPerson extends Model
{
    use HasFactory;

    public function trucking()
    {
      return $this->belongsTo('App\Models\TruckingCompany', 'tc_id');
    }

    public function detail()
    {
        return $this->hasMany('App\Models\TruckingCompanyContact', 'tc_cp_id', 'id');
    }
}

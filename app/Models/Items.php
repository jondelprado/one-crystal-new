<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    use HasFactory;

    public function customer_item()
    {
      return $this->hasMany('App\Models\LocalCustomerItem', 'item_id', 'id');
    }

    public function container()
    {
      return $this->belongsTo('App\Models\Containers', 'id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Containers extends Model
{
    use HasFactory;

    public function shipper()
    {
        return $this->hasOne('App\Models\ForeignPartnersShipper', 'id', 'ic_shipper_id');
    }

    public function consignee()
    {
        return $this->hasOne('App\Models\CustomsBrokerageConsignee', 'id', 'ic_consignee_id');
    }

    public function shipping()
    {
        return $this->hasOne('App\Models\ShippingLine', 'id', 'ic_shipping_id');
    }

    public function loading()
    {
        return $this->hasOne('App\Models\Ports', 'id', 'ic_loading_port');
    }

    public function discharge()
    {
        return $this->hasOne('App\Models\Ports', 'id', 'ic_discharge_port');
    }

    public function trucking()
    {
        return $this->hasOne('App\Models\TruckingCompany', 'id', 'ic_trucking_company');
    }

    public function item()
    {
        return $this->hasOne('App\Models\Items', 'id', 'ic_items');
    }

    public function customer()
    {
        return $this->hasOne('App\Models\LocalCustomer', 'id', 'ic_customer');
    }

    public function encoder()
    {
        return $this->hasOne('App\Models\User', 'id', 'ic_encoded_by');
    }

    public function recorder()
    {
        return $this->hasOne('App\Models\User', 'id', 'ic_eta_acfta_by');
    }
}



















//

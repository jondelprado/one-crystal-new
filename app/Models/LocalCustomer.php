<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocalCustomer extends Model
{
    use HasFactory;

    public function address()
    {
        return $this->hasMany('App\Models\LocalCustomerAddress', 'lc_id', 'id');
    }

    public function contact()
    {
        return $this->hasMany('App\Models\LocalCustomerContactPerson', 'lc_id', 'id');
    }

    public function item()
    {
        return $this->hasMany('App\Models\LocalCustomerItem', 'lc_id', 'id');
    }

    public function container()
    {
      return $this->belongsTo('App\Models\Containers', 'id');
    }
}

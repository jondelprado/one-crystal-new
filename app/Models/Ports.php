<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ports extends Model
{
    use HasFactory;

    public function container()
    {
      return $this->belongsTo('App\Models\Containers', 'id');
    }
}

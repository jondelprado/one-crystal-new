<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TruckingCompanyAddress extends Model
{
    use HasFactory;

    public function trucking(){
      return $this->belongsTo('App\Models\TruckingCompany', 'tc_id');
    }
}

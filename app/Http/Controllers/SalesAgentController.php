<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\SalesAgent;
use App\Models\SalesAgentAddress;
use App\Models\SalesAgentContact;

class SalesAgentController extends Controller
{

  public function index(){

    $selected_status = Session::get("sa_status");
    $status = ($selected_status != "") ? $selected_status : "Active";
    $get_sales_agent = SalesAgent::with("address", "contact", "customer")->where("status", $status)->get();

    return view('profiles.sales-agent.index', compact('get_sales_agent', 'status'));
  }

  public function store(Request $request){

    $agent_name = $request->agent_name;
    $agent_address_type = $request->agent_address_type;
    $agent_address = $request->agent_address;
    $agent_city = $request->agent_city;
    $agent_contact_type = $request->agent_contact_type;
    $agent_contact = $request->agent_contact;
    $agent_comm = str_replace(",","",$request->agent_comm);
    $agent_note = $request->agent_note;

    $new_agent = new SalesAgent();
    $new_agent->sa_name = $agent_name;
    $new_agent->sa_commission = (float)$agent_comm;
    $new_agent->sa_note = $agent_note;
    $new_agent->status = "Active";
    $new_agent->save();
    $new_agent_id = $new_agent->id;

    foreach($agent_address_type as $add_index => $add_type) {
      $insert_address[] = [
        "sa_address_type" => $add_type,
        "sa_address" => $agent_address[$add_index],
        "sa_city" => $agent_city[$add_index],
        "sa_id" => $new_agent_id,
      ];
    }

    foreach($agent_contact_type as $con_index => $con_type) {
      $insert_contact[] = [
        "sa_contact_type" => $con_type,
        "sa_contact" => $agent_contact[$con_index],
        "sa_id" => $new_agent_id,
      ];
    }

    SalesAgentAddress::insert($insert_address);
    SalesAgentContact::insert($insert_contact);

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function edit(Request $request){

    $type = $request->type;
    $agent_id = $request->agent_id;

    if ($type == "main_details") {

      $agent_name = $request->edit_agent_name;
      $agent_comm = str_replace(",","",$request->edit_agent_comm);
      $agent_note = $request->edit_agent_note;
      $agent_address_type = $request->edit_agent_address_type;
      $agent_address = $request->edit_agent_address;
      $agent_city = $request->edit_agent_city;
      $agent_contact_type = $request->edit_agent_contact_type;
      $agent_contact = $request->edit_agent_contact;

      SalesAgentContact::where("sa_id", $agent_id)->delete();
      SalesAgentAddress::where("sa_id", $agent_id)->delete();
      SalesAgent::where("id", $agent_id)->update(array(
        "sa_name" => $agent_name,
        "sa_commission" => (float)$agent_comm,
        "sa_note" => $agent_note,
      ));

      foreach($agent_address_type as $add_index => $add_type) {
        $insert_address[] = [
          "sa_address_type" => $add_type,
          "sa_address" => $agent_address[$add_index],
          "sa_city" => $agent_city[$add_index],
          "sa_id" => $agent_id,
        ];
      }

      foreach($agent_contact_type as $con_index => $con_type) {
        $insert_contact[] = [
          "sa_contact_type" => $con_type,
          "sa_contact" => $agent_contact[$con_index],
          "sa_id" => $agent_id ,
        ];
      }

      SalesAgentAddress::insert($insert_address);
      SalesAgentContact::insert($insert_contact);
    }
    elseif ($type == "add_details") {

      $add_address_type = $request->edit_agent_address_type;
      $add_address = $request->edit_agent_address;
      $add_city = $request->edit_agent_city;
      $add_contact_type = $request->edit_agent_contact_type;
      $add_contact = $request->edit_agent_contact;

      if ($add_address_type && $add_address && $add_city) {
        foreach($add_address_type as $add_index => $add_type) {
          $insert_address[] = [
            "sa_address_type" => $add_type,
            "sa_address" => $add_address[$add_index],
            "sa_city" => $add_city[$add_index],
            "sa_id" => $agent_id,
          ];
        }

        SalesAgentAddress::insert($insert_address);
      }

      if ($add_contact_type && $add_contact) {
        foreach($add_contact_type as $con_index => $con_type) {
          $insert_contact[] = [
            "sa_contact_type" => $con_type,
            "sa_contact" => $add_contact[$con_index],
            "sa_id" => $agent_id,
          ];
        }
        SalesAgentContact::insert($insert_contact);
      }

    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function retire(Request $request){

    $agent_id = $request->get_agent_id;

    SalesAgent::where("id", $agent_id)->update(array("status" => "Retired"));

    return response()->json(array(
      "status" => "success",
    ));

  }

  public function removeDetail(Request $request){

    $get_type = $request->type;

    if ($get_type == "address") {
      SalesAgentAddress::where("id", $request->get_address_id)->delete();
    }
    else {
      SalesAgentContact::where("id", $request->get_contact_id)->delete();
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

}



























//

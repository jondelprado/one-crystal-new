<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\SalesAgent;
use App\Models\LocalCustomer;
use App\Models\LocalCustomerContact;
use App\Models\LocalCustomerItem;
use App\Models\ForeignPartners;
use App\Models\ForeignPartnersContact;
use App\Models\CustomsBrokerage;
use App\Models\CustomsBrokerageContact;
use App\Models\TruckingCompany;
use App\Models\TruckingCompanyContact;
use App\Models\ShippingLine;
use App\Models\ShippingLineContact;
use App\Models\Ports;
use App\Models\Items;
use App\Models\Containers;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function additionalContactDetail(Request $request){

      $type = $request->type;
      $id = $request->id;
      $contact_type = $request->contact_type;
      $contact_detail = $request->contact_detail;

      switch ($type) {
        case 'lc':
          $new_contact = new LocalCustomerContact();
          $new_contact->lc_cp_contact_type = $contact_type;
          $new_contact->lc_cp_contact = $contact_detail;
          $new_contact->lc_cp_id = $id;
          $new_contact->save();
          break;

        case 'fp':
          $new_contact = new ForeignPartnersContact();
          $new_contact->fp_cp_contact_type = $contact_type;
          $new_contact->fp_cp_contact = $contact_detail;
          $new_contact->fp_cp_id = $id;
          $new_contact->save();
          break;

        case 'cb':
          $new_contact = new CustomsBrokerageContact();
          $new_contact->cb_cp_contact_type = $contact_type;
          $new_contact->cb_cp_contact = $contact_detail;
          $new_contact->cb_cp_id = $id;
          $new_contact->save();
          break;

        case 'tc':
          $new_contact = new TruckingCompanyContact();
          $new_contact->tc_cp_contact_type = $contact_type;
          $new_contact->tc_cp_contact = $contact_detail;
          $new_contact->tc_cp_id = $id;
          $new_contact->save();
          break;

        default:
          $new_contact = new ShippingLineContact();
          $new_contact->sl_cp_contact_type = $contact_type;
          $new_contact->sl_cp_contact = $contact_detail;
          $new_contact->sl_cp_id = $id;
          $new_contact->save();
          break;
      }

      return response()->json(array(
        "status" => "success",
      ));
    }

    public function containerExpenses(Request $request){

      $id = $request->id;
      $type = $request->type;
      $value = $request->value;
      $value2 = $request->value2;

      switch ($type) {
        case 'price':
          $explode_val = explode("-", $value2);
          $item = $explode_val[0];
          $customer = $explode_val[1];

          Containers::where("id", $id)->update(array(
            "ic_package_price" => $value,
          ));

          LocalCustomerItem::where("item_id", $item)
          ->where("lc_id", $customer)
          ->where("ic_id", $id)
          ->update(array(
            "package_price" => $value,
          ));
          $title = "Package Price";
          break;

        case 'cost':
          Containers::where("id", $id)->update(array(
            "ic_package_cost" => $value,
          ));
          $title = "Package Cost";
          break;

        default:
          Containers::where("id", $id)->update(array(
            "ic_trucking_charge" => $value,
          ));
          $title = "Trucking Charge";
          break;
      }

      return response()->json(array(
        "status" => "success",
        "title" => $title,
      ));
    }

    public function singleSetStatus(Request $request){

      $type = $request->type;
      $id = $request->id;
      $status = $request->status;

      switch ($type) {
        case 'sa':
          SalesAgent::where("id", $id)->update(array("status" => $status));
          break;

        case "lc":
          LocalCustomer::where("id", $id)->update(array("status" => $status));
          break;

        case "fp":
          ForeignPartners::where("id", $id)->update(array("status" => $status));
          break;

        case "cb":
          CustomsBrokerage::where("id", $id)->update(array("status" => $status));
          break;

        case "tc":
          TruckingCompany::where("id", $id)->update(array("status" => $status));
          break;

        case "sl":
          ShippingLine::where("id", $id)->update(array("status" => $status));
          break;

        case "port":
          Ports::where("id", $id)->update(array("status" => $status));
          break;

        default:
          Items::where("id", $id)->update(array("status" => $status));
          break;
      }

      return response()->json(array(
        "status" => "success",
      ));
    }

    public function multipleSetStatus(Request $request){

      $request_type = $request->request_type;
      $status = $request->status;
      date_default_timezone_set('Asia/Manila');
      $current_timestamp = date('Y-m-d H:i:s');

      switch ($request_type) {
        case 'sa_request':
          SalesAgent::whereIn("id", $request->select_sa)->update(array("status" => $status));
          break;

        case 'lc_request':
          LocalCustomer::whereIn("id", $request->select_lc)->update(array("status" => $status));
          break;

        case 'fp_request':
          ForeignPartners::whereIn("id", $request->select_fp)->update(array("status" => $status));
          break;

        case 'cb_request':
          CustomsBrokerage::whereIn("id", $request->select_cb)->update(array("status" => $status));
          break;

        case 'tc_request':
          TruckingCompany::whereIn("id", $request->select_tc)->update(array("status" => $status));
          break;

        case 'sl_request':
          ShippingLine::whereIn("id", $request->select_sl)->update(array("status" => $status));
          break;

        case 'port_request':
          Ports::whereIn("id", $request->select_port)->update(array("status" => $status));
          break;

        case 'item_request':
          Items::whereIn("id", $request->select_item)->update(array("status" => $status));
          break;

        case 'ic_request':
          Containers::whereIn("id", $request->select_ic)->update(array(
            "status" => "Deleted",
            "deleted_at" => $current_timestamp
          ));
          break;

        default:
          // code...
          break;
      }

      return response()->json(array(
        "status" => "success",
      ));
    }

    public function multipleProceed(Request $request){

      $request_type = $request->request_type;
      $current_date = date('Y-m-d');

      switch ($request_type) {
        case 'to_on_process':
          Containers::whereIn("id", $request->select_ic)->update(array(
            "ic_status" => "On-Process",
            "ic_on_process_date" => $current_date
          ));
          break;

        case 'to_trucking':
          Containers::whereIn("id", $request->select_op)->update(array(
            "ic_status" => "Endorsed-Trucking",
            "ic_endorsed_date" => $current_date
          ));
          break;

        case 'to_delivered':
          Containers::whereIn("id", $request->select_et)->update(array(
            "ic_status" => "Delivered",
            "ic_delivery_date" => $current_date
          ));

          $get_containers = Containers::whereIn("id", $request->select_et)->get();

          foreach ($get_containers as $container) {
            $item = $container->ic_items;
            $customer = $container->ic_customer;

            LocalCustomerItem::where("lc_id", $customer)
            ->where("ic_id", $container->id)
            ->where("item_id", $item)
            ->update(array(
              "last_delivery_date" => $current_date,
            ));
          }
          break;

        default:
          // code...
          break;
      }

      return response()->json(array(
        "status" => "success",
      ));
    }

    public function multipleRevert(Request $request){

      $request_type = $request->request_type;
      $current_date = date('Y-m-d');

      switch ($request_type) {

        case 'to_incoming':
          Containers::whereIn("id", $request->select_op)->update(array(
            "ic_status" => "Incoming",
          ));
          break;

        case 'to_on_process':
          Containers::whereIn("id", $request->select_et)->update(array(
            "ic_status" => "On-Process",
          ));
          break;

        default:
          // code...
          break;
      }

      return response()->json(array(
        "status" => "success",
      ));
    }

    public function setDaterange(Request $request){

      if ($request->request_type == "ic_daterange") {
        Session::forget('ic_daterange');
        Session::put('ic_daterange', $request->start."/".$request->end);
      }
      else {
        Session::forget('dc_daterange');
        Session::put('dc_daterange', $request->start."/".$request->end);
      }

      return response()->json(array(
        "status" => "success",
      ));
    }

    public function setStatus(Request $request){

      switch ($request->request_type) {
        case 'sa_filter':
          Session::forget('sa_status');
          Session::put('sa_status', $request->status);
          break;

        case 'lc_filter':
          Session::forget('lc_status');
          Session::put('lc_status', $request->status);
          break;

        case 'fp_filter':
          Session::forget('fp_status');
          Session::put('fp_status', $request->status);
          break;

        case 'cb_filter':
          Session::forget('cb_status');
          Session::put('cb_status', $request->status);
          break;

        case 'tc_filter':
          Session::forget('tc_status');
          Session::put('tc_status', $request->status);
          break;

        case 'sl_filter':
          Session::forget('sl_status');
          Session::put('sl_status', $request->status);
          break;

        case 'port_filter':
          Session::forget('port_status');
          Session::put('port_status', $request->status);
          break;

        case 'item_filter':
          Session::forget('item_status');
          Session::put('item_status', $request->status);
          break;

        default:
          // code...
          break;
      }

    }
}




























//

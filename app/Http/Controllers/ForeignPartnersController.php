<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\ForeignPartners;
use App\Models\ForeignPartnersAddress;
use App\Models\ForeignPartnersContactPerson;
use App\Models\ForeignPartnersContact;
use App\Models\ForeignPartnersShipper;
use Monarobase\CountryList\CountryListFacade;

class ForeignPartnersController extends Controller
{

  public function index(){

    $get_fp_details = $countries = [];

    $selected_status = Session::get("fp_status");
    $status = ($selected_status != "") ? $selected_status : "Active";

    $get_foreign_partners = ForeignPartners::with(["address", "contact.detail" => function($sub_query){
      $sub_query->where("fp_cp_contact_type", "Mobile")->orWhere("fp_cp_contact_type", "Email");
    }])->where("status", $status)->get();

    if ($status == "Active") {
      $get_fp_details = ForeignPartners::with("address", "contact.detail", "shipper")->where("status", $status)->get();
      $countries = CountryListFacade::getList('en');
    }

    return view('profiles.foreign-partners.index', compact('countries', 'get_foreign_partners', 'get_fp_details', 'status'));
  }

  public function store(Request $request){

    $fp_name = $request->fp_name;
    $fp_address_arr = $request->fp_address;
    $fp_cp_arr = $request->fp_contact_person;
    $fp_type = $request->fp_type;
    $fp_note = $request->fp_note;

    $new_fp = new ForeignPartners();
    $new_fp->fp_name = $fp_name;
    $new_fp->fp_type = $fp_type;
    $new_fp->fp_note = $fp_note;
    $new_fp->status = "Active";
    $new_fp->save();
    $new_fp_id = $new_fp->id;

    foreach ($fp_address_arr as $fp_address) {
      $explode_address = explode(",", $fp_address);

      foreach ($explode_address as $fp_add) {
        $explode = explode("/", $fp_add);
        $address_type = $explode[0];
        $address = $explode[1];
        $country = $explode[2];

        $insert_address[] = [
          "fp_address_type" => $address_type,
          "fp_address" => $address,
          "fp_country" => $country,
          "fp_id" => $new_fp_id,
        ];
      }
    }
    ForeignPartnersAddress::insert($insert_address);

    foreach ($fp_cp_arr as $fp_cp) {
      $explode_cp = explode(",", $fp_cp);

      foreach ($explode_cp as $cp) {
        $explode_val = explode("/", $cp);
        $arr_count = count($explode_val);
        $cp_name = $explode_val[0];
        $cp_position = $explode_val[1];

        $new_cp = new ForeignPartnersContactPerson();
        $new_cp->fp_cp_name = $cp_name;
        $new_cp->fp_cp_position = $cp_position;
        $new_cp->fp_id = $new_fp_id;
        $new_cp->save();
        $new_cp_id = $new_cp->id;

        for ($i=2; $i <= $arr_count-1; $i++) {
          $explode_cp_contact = explode("-", $explode_val[$i]);
          $contact_type = $explode_cp_contact[0];
          $contact = $explode_cp_contact[1];

          $insert_contact[] = [
            "fp_cp_contact_type" => $contact_type,
            "fp_cp_contact" => $contact,
            "fp_cp_id" => $new_cp_id,
          ];
        }
      }
      ForeignPartnersContact::insert($insert_contact);
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function edit(Request $request){

    $type = $request->type;
    $fp_id = $request->fp_id;

    if ($type == "main_details") {

      $fp_name = $request->fp_name;
      $fp_address_arr = $request->fp_address;
      $fp_cp_arr = $request->fp_contact_person;
      $fp_type = $request->fp_type;
      $fp_note = $request->fp_note;

      ForeignPartners::where("id", $fp_id)->update(array(
        "fp_name" => $fp_name,
        "fp_type" => $fp_type,
        "fp_note" => $fp_note,
      ));

      foreach ($fp_address_arr as $fp_address) {
        $explode_address = explode(",", $fp_address);

        foreach ($explode_address as $fp_add) {
          $explode = explode("/", $fp_add);
          $address_id = $explode[0];
          $address_type = $explode[1];
          $address = $explode[2];
          $country = $explode[3];

          ForeignPartnersAddress::where("id", $address_id)->update(array(
            "fp_address_type" => $address_type,
            "fp_address" => $address,
            "fp_country" => $country,
          ));
        }
      }

      foreach ($fp_cp_arr as $fp_cp) {
        $explode_cp = explode(",", $fp_cp);

        foreach ($explode_cp as $cp) {
          $explode_val = explode("/", $cp);
          $arr_count = count($explode_val);
          $cp_id = $explode_val[0];
          $cp_name = $explode_val[1];
          $cp_position = $explode_val[2];

          ForeignPartnersContactPerson::where("id", $cp_id)->update(array(
            "fp_cp_name" => $cp_name,
            "fp_cp_position" => $cp_position,
          ));

          for ($i=3; $i <= $arr_count-1; $i++) {
            $explode_cp_contact = explode("-", $explode_val[$i]);
            $contact_type = $explode_cp_contact[0];
            $contact = $explode_cp_contact[1];
            $contact_id = $explode_cp_contact[2];

            ForeignPartnersContact::where("id", $contact_id)->update(array(
              "fp_cp_contact_type" => $contact_type,
              "fp_cp_contact" => $contact,
            ));
          }
        }
      }

    }
    elseif ($type == "add_details") {

      $fp_address_arr = $request->fp_address;
      $fp_cp_arr = $request->fp_contact_person;

      if (array_filter($fp_address_arr) != []) {
        foreach ($fp_address_arr as $fp_address) {
          $explode_address = explode(",", $fp_address);

          foreach ($explode_address as $fp_add) {
            $explode = explode("/", $fp_add);
            $address_type = $explode[0];
            $address = $explode[1];
            $country = $explode[2];

            $insert_address[] = [
              "fp_address_type" => $address_type,
              "fp_address" => $address,
              "fp_country" => $country,
              "fp_id" => $fp_id,
            ];
          }
        }
        ForeignPartnersAddress::insert($insert_address);
      }

      if (array_filter($fp_cp_arr) != []) {
        foreach ($fp_cp_arr as $fp_cp) {
          $explode_cp = explode(",", $fp_cp);

          foreach ($explode_cp as $cp) {
            $explode_val = explode("/", $cp);
            $arr_count = count($explode_val);
            $cp_name = $explode_val[0];
            $cp_position = $explode_val[1];

            $new_cp = new ForeignPartnersContactPerson();
            $new_cp->fp_cp_name = $cp_name;
            $new_cp->fp_cp_position = $cp_position;
            $new_cp->fp_id = $fp_id;
            $new_cp->save();
            $new_cp_id = $new_cp->id;

            for ($i=2; $i <= $arr_count-1; $i++) {
              $explode_cp_contact = explode("-", $explode_val[$i]);
              $contact_type = $explode_cp_contact[0];
              $contact = $explode_cp_contact[1];

              $insert_contact[] = [
                "fp_cp_contact_type" => $contact_type,
                "fp_cp_contact" => $contact,
                "fp_cp_id" => $new_cp_id,
              ];
            }
          }
          ForeignPartnersContact::insert($insert_contact);
        }
      }

    }
    else {

      $fp_shipper_arr = $request->fp_shipper;

      if (array_filter($fp_shipper_arr) != []) {

        ForeignPartnersShipper::where("fp_id", $fp_id)->delete();

        foreach ($fp_shipper_arr as $fp_shipper) {
          $explode_shipper = explode(",", $fp_shipper);

          foreach ($explode_shipper as $fp_sc) {
            $explode = explode("/", $fp_sc);
            $name = $explode[0];
            $address = $explode[1];

            $insert_shipper[] = [
              "fp_sc_name" => $name,
              "fp_sc_address" => $address,
              "fp_id" => $fp_id,
            ];
          }
        }
        ForeignPartnersShipper::insert($insert_shipper);
      }

    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function removeDetail(Request $request){

    $get_type = $request->type;

    if ($get_type == "address") {
      ForeignPartnersAddress::where("id", $request->get_address_id)->delete();
    }
    elseif ($get_type == "person") {
      ForeignPartnersContact::where("fp_cp_id", $request->get_person_id)->delete();
      ForeignPartnersContactPerson::where("id", $request->get_person_id)->delete();
    }
    elseif ($get_type == "contact") {
      ForeignPartnersContact::where("id", $request->get_contact_id)->delete();
    }
    else {
      ForeignPartnersShipper::where("id", $request->get_shipper_id)->delete();
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

}











































//

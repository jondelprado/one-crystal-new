<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Models\Containers;
use App\Models\User;

class OnProcessController extends Controller
{

  public function index(){

    $current_date = date('Y-m-d');
    $get_on_process = Containers::with("shipper","consignee","shipping",
                                "loading","discharge","item",
                                "customer","encoder","recorder")
                              ->where("ic_status", "On-Process")
                              ->where("status", "Active")
                              ->get();

    return view('transactions.on-process.index', compact('get_on_process', 'current_date'));
  }

  public function additionalDetail(Request $request){

    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $user = Auth::guard()->user();
    $user_id = $user->id;
    $ic_id = $request->ic_id;
    $request_type = $request->request_type;
    $response = array(
      "status" => "success",
    );

    function generateRandomString($length) {
      return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    $random_str = generateRandomString(10);

    switch ($request_type) {
      case 'eta_date':
        $query = array(
          "ic_eta" => $request->get_eta_date,
          "ic_eta_acfta_by" => $user_id
        );
        $response["eta_date_raw"] = $request->get_eta_date;
        $response["eta_date"] = date("m-d-Y", strtotime($request->get_eta_date));
        break;

      case 'yard_date':
        $query = array(
          "ic_yard_date" => $request->get_yard_date,
        );
        $response["yard_date_raw"] = $request->get_yard_date;
        $response["yard_date"] = date("m-d-Y", strtotime($request->get_yard_date));
        break;

      case 'sl_payment':
        $query = array(
          "ic_sl_payment_date" => $request->get_sl_date,
          "ic_sl_expenses" => $request->get_sl_expenses,
          "ic_sl_container_deposit" => $request->get_sl_deposit,
        );
        $response["sl_date_raw"] = $request->get_sl_date;
        $response["sl_date"] = date("m-d-Y", strtotime($request->get_sl_date));
        $response["sl_expenses"] = $request->get_sl_expenses;
        $response["sl_deposit"] = $request->get_sl_deposit;
        break;

      case 'lodge_date':
        $query = array(
          "ic_lodge_date" => $request->get_lodge_date,
        );
        $response["lodge_date_raw"] = $request->get_lodge_date;
        $response["lodge_date"] = date("m-d-Y", strtotime($request->get_lodge_date));
        break;

      case 'final_duties':
        $query = array(
          "ic_final_date" => $request->get_fd_date,
          "ic_final_duties" => $request->get_fd_duties,
        );
        $response["fd_date_raw"] = $request->get_fd_date;
        $response["fd_date"] = date("m-d-Y", strtotime($request->get_fd_date));
        $response["fd_duties"] = $request->get_fd_duties;
        break;

      case 'debited_duties':
        $query = array(
          "ic_debited_date" => $request->get_dd_date,
          "ic_debited_duties" => $request->get_dd_duties,
        );
        $response["dd_date_raw"] = $request->get_dd_date;
        $response["dd_date"] = date("m-d-Y", strtotime($request->get_dd_date));
        $response["dd_duties"] = $request->get_dd_duties;
        break;

      case 'cro_date':
        $query = array(
          "ic_cro_validity" => $request->get_cro_date,
        );
        $response["cro_date_raw"] = $request->get_cro_date;
        $response["cro_date"] = date("m-d-Y", strtotime($request->get_cro_date));
        break;

      case 'storage_date':
        $query = array(
          "ic_storage_validity" => $request->get_storage_date,
        );
        $response["storage_date_raw"] = $request->get_storage_date;
        $response["storage_date"] = date("m-d-Y", strtotime($request->get_storage_date));
        break;

      case 'oft_time':
        $query = array(
          "ic_oft" => $request->get_time,
        );
        $response["time_raw"] = $request->get_time;
        $response["time"] = date("h:i A", strtotime($request->get_time));
        break;

      case 'lft_time':
        $query = array(
          "ic_lft" => $request->get_time,
        );
        $response["time_raw"] = $request->get_time;
        $response["time"] = date("h:i A", strtotime($request->get_time));
        break;

      case 'reg_no':
        $query = array(
          "ic_reg_no" => $request->get_no,
        );
        $response["no"] = $request->get_no;
        break;

      case 'entry_no':
        $query = array(
          "ic_entry_no" => $request->get_no,
        );
        $response["no"] = $request->get_no;
        break;

      case 'pro_no':
        $query = array(
          "ic_pro_no" => $request->get_no,
        );
        $response["no"] = $request->get_no;
        break;

      case 'upload_gp':
        $file = $request->file('get_gp_file');
        $name = $file->getClientOriginalName();
        $file->move(public_path('storage/incoming-container-files/'), 'GP_'.$ic_id.'_'.$date.'_'.$random_str.'_'.$name);
        $url = 'storage/incoming-container-files/GP_'.$ic_id.'_'.$date.'_'.$random_str.'_'.$name;
        $query = array(
          "ic_gate_date" => $request->get_gp_date,
          "ic_gate_pass_file" => $url,
        );
        $response["gp_date_raw"] = $request->get_gp_date;
        $response["gp_date"] = date("m-d-Y", strtotime($request->get_gp_date));
        $response["gp_file"] = $url;
        break;

      default:

        break;
    }

    Containers::where("id", $ic_id)->update($query);

    return response()->json($response);
  }

}







































//

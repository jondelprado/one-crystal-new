<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\ShippingLine;
use App\Models\ShippingLineAddress;
use App\Models\ShippingLineContactPerson;
use App\Models\ShippingLineContact;

class ShippingLineController extends Controller
{

  public function index(){

    $get_sl_details = [];

    $selected_status = Session::get("sl_status");
    $status = ($selected_status != "") ? $selected_status : "Active";

    $get_shipping_lines = ShippingLine::with(["address", "contact.detail" => function($sub_query){
      $sub_query->where("sl_cp_contact_type", "Mobile")->orWhere("sl_cp_contact_type", "Email");
    }])->where("status", $status)->get();

    if ($status == "Active") {
      $get_sl_details = ShippingLine::with("address", "contact.detail")->where("status", $status)->get();
    }

    return view('profiles.shipping-line.index', compact('get_shipping_lines', 'get_sl_details', 'status'));
  }

  public function store(Request $request){

    $sl_name = $request->sl_name;
    $sl_address_arr = $request->sl_address;
    $sl_cp_arr = $request->sl_contact_person;
    $sl_terms = $request->sl_terms;
    $sl_note = $request->sl_note;

    $new_sl = new ShippingLine();
    $new_sl->sl_name = $sl_name;
    $new_sl->sl_terms = $sl_terms;
    $new_sl->sl_note = $sl_note;
    $new_sl->status = "Active";
    $new_sl->save();
    $new_sl_id = $new_sl->id;

    foreach ($sl_address_arr as $sl_address) {
      $explode_address = explode(",", $sl_address);

      foreach ($explode_address as $sl_add) {
        $explode = explode("/", $sl_add);
        $address_type = $explode[0];
        $address = $explode[1];
        $city = $explode[2];

        $insert_address[] = [
          "sl_address_type" => $address_type,
          "sl_address" => $address,
          "sl_city" => $city,
          "sl_id" => $new_sl_id,
        ];
      }
    }
    ShippingLineAddress::insert($insert_address);

    foreach ($sl_cp_arr as $sl_cp) {
      $explode_cp = explode(",", $sl_cp);

      foreach ($explode_cp as $cp) {
        $explode_val = explode("/", $cp);
        $arr_count = count($explode_val);
        $cp_name = $explode_val[0];
        $cp_position = $explode_val[1];

        $new_cp = new ShippingLineContactPerson();
        $new_cp->sl_cp_name = $cp_name;
        $new_cp->sl_cp_position = $cp_position;
        $new_cp->sl_id = $new_sl_id;
        $new_cp->save();
        $new_cp_id = $new_cp->id;

        for ($i=2; $i <= $arr_count-1; $i++) {
          $explode_cp_contact = explode("-", $explode_val[$i]);
          $contact_type = $explode_cp_contact[0];
          $contact = $explode_cp_contact[1];

          $insert_contact[] = [
            "sl_cp_contact_type" => $contact_type,
            "sl_cp_contact" => $contact,
            "sl_cp_id" => $new_cp_id,
          ];
        }
      }
      ShippingLineContact::insert($insert_contact);
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function edit(Request $request){

    $type = $request->type;
    $sl_id = $request->sl_id;

    if ($type == "main_details") {

      $sl_name = $request->sl_name;
      $sl_address_arr = $request->sl_address;
      $sl_cp_arr = $request->sl_contact_person;
      $sl_terms = $request->sl_terms;
      $sl_note = $request->sl_note;

      ShippingLine::where("id", $sl_id)->update(array(
        "sl_name" => $sl_name,
        "sl_terms" => $sl_terms,
        "sl_note" => $sl_note,
      ));

      foreach ($sl_address_arr as $sl_address) {
        $explode_address = explode(",", $sl_address);

        foreach ($explode_address as $sl_add) {
          $explode = explode("/", $sl_add);
          $address_id = $explode[0];
          $address_type = $explode[1];
          $address = $explode[2];
          $city = $explode[3];

          ShippingLineAddress::where("id", $address_id)->update(array(
            "sl_address_type" => $address_type,
            "sl_address" => $address,
            "sl_city" => $city,
          ));
        }
      }

      foreach ($sl_cp_arr as $sl_cp) {
        $explode_cp = explode(",", $sl_cp);

        foreach ($explode_cp as $cp) {
          $explode_val = explode("/", $cp);
          $arr_count = count($explode_val);
          $cp_id = $explode_val[0];
          $cp_name = $explode_val[1];
          $cp_position = $explode_val[2];

          ShippingLineContactPerson::where("id", $cp_id)->update(array(
            "sl_cp_name" => $cp_name,
            "sl_cp_position" => $cp_position,
          ));

          for ($i=3; $i <= $arr_count-1; $i++) {
            $explode_cp_contact = explode("-", $explode_val[$i]);
            $contact_type = $explode_cp_contact[0];
            $contact = $explode_cp_contact[1];
            $contact_id = $explode_cp_contact[2];

            ShippingLineContact::where("id", $contact_id)->update(array(
              "sl_cp_contact_type" => $contact_type,
              "sl_cp_contact" => $contact,
            ));
          }
        }
      }

    }
    elseif ($type == "add_details") {

      $sl_address_arr = $request->sl_address;
      $sl_cp_arr = $request->sl_contact_person;

      if (array_filter($sl_address_arr) != []) {
        foreach ($sl_address_arr as $sl_address) {
          $explode_address = explode(",", $sl_address);

          foreach ($explode_address as $sl_add) {
            $explode = explode("/", $sl_add);
            $address_type = $explode[0];
            $address = $explode[1];
            $city = $explode[2];

            $insert_address[] = [
              "sl_address_type" => $address_type,
              "sl_address" => $address,
              "sl_city" => $city,
              "sl_id" => $sl_id,
            ];
          }
        }
        ShippingLineAddress::insert($insert_address);
      }

      if (array_filter($sl_cp_arr) != []) {
        foreach ($sl_cp_arr as $sl_cp) {
          $explode_cp = explode(",", $sl_cp);

          foreach ($explode_cp as $cp) {
            $explode_val = explode("/", $cp);
            $arr_count = count($explode_val);
            $cp_name = $explode_val[0];
            $cp_position = $explode_val[1];

            $new_cp = new ShippingLineContactPerson();
            $new_cp->sl_cp_name = $cp_name;
            $new_cp->sl_cp_position = $cp_position;
            $new_cp->sl_id = $sl_id;
            $new_cp->save();
            $new_cp_id = $new_cp->id;

            for ($i=2; $i <= $arr_count-1; $i++) {
              $explode_cp_contact = explode("-", $explode_val[$i]);
              $contact_type = $explode_cp_contact[0];
              $contact = $explode_cp_contact[1];

              $insert_contact[] = [
                "sl_cp_contact_type" => $contact_type,
                "sl_cp_contact" => $contact,
                "sl_cp_id" => $new_cp_id,
              ];
            }
          }
          ShippingLineContact::insert($insert_contact);
        }
      }

    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function removeDetail(Request $request){

    $get_type = $request->type;

    if ($get_type == "address") {
      ShippingLineAddress::where("id", $request->get_address_id)->delete();
    }
    elseif ($get_type == "person") {
      ShippingLineContact::where("sl_cp_id", $request->get_person_id)->delete();
      ShippingLineContactPerson::where("id", $request->get_person_id)->delete();
    }
    elseif ($get_type == "contact") {
      ShippingLineContact::where("id", $request->get_contact_id)->delete();
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

}

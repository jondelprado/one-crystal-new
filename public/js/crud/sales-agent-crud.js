//ADD AGENT
$(".btn_done").on("click", function(){

  var empty_count = 0;

  $(".required_fields").each(function(){
    if ($(this).val() == "") {
      empty_count++;
    }
  });

  if (empty_count == 0) {

    var form = new FormData();

    $(".required_fields").each(function(){
      form.append($(this).attr("name"), $(this).val());
    });

    $.ajax({
      type: "post",
      url: "/store-agent",
      processData: false,
      contentType: false,
      cache: false,
      data: form,
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successSave();
            console.log(data.test);
          }
        }, 1500);
      },
    });

  }
  else {
    emptyFields("required_fields");
  }

});

//EDIT SALES AGENT
$(".btn_save").on("click", function(){

  var empty_count = 0;
  var get_detail_type = $(this).attr("id");
  var get_agent_id = $(this).attr("data-target");
  var field_type;
  var form = new FormData();

  if (get_detail_type == "main_details") {
    $(".edit_required_fields_"+get_agent_id).each(function(){
      if ($(this).val() == "") {
        empty_count++;
      }
      else {
        form.append($(this).attr("name"), $(this).val());
      }
    });
    field_type = "edit_required_fields_"+get_agent_id;
  }
  else if (get_detail_type == "add_details") {
    $(".additional_fields_"+get_agent_id).each(function(){
      if ($(this).val() != "") {
        form.append($(this).attr("name"), $(this).val());
      }
    });
    field_type = "additional_fields_"+get_agent_id;
  }

  if (empty_count == 0) {

    form.append("type", get_detail_type);
    form.append("agent_id", get_agent_id);

    swal.fire({
      type: "info",
      title: "Save Changes",
      confirmButtonColor: 'lightgreen',
      cancelButtonColor: 'red',
      confirmButtonText: '<span style="color: white;">Proceed</span>',
      cancelButtonText: '<span style="color: white;">Cancel</span>',
      reverseButtons: true,
      showCancelButton: true,
      html: "<b class='swal_info'>Are you sure you want to save these changes?</b>",
    }).then((result) => {
      if (result.value) {
        $.ajax({
          type: "post",
          url: "/edit-agent",
          processData: false,
          contentType: false,
          cache: false,
          data: form,
          dataType: "json",
          beforeSend: function(){
            loader();
          },
          success: function(data){
            setTimeout(function(){
              if (data.status == "success") {
                successEdit();
              }
            }, 1500);
          },
        });
      }
    });

  }
  else {
    emptyFields(field_type);
  }

});

//REMOVE CURRENT ADDRESS
$(".remove_current_address").on("click", function(){

  var get_address_id = $(this).attr("data-target");
  var type = "address";

  swal.fire({
    type: "warning",
    title: "Remove Address",
    html: "<b>Are you sure you want to remove this address?</b>",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/remove-detail-agent",
        data: {get_address_id: get_address_id, type: type},
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              successRemove();
              $(".address_"+get_address_id).remove();
            }
          }, 1500);
        },
      });
    }
  });

});

//REMOVE CURRENT CONTACT
$(".remove_current_contact").on("click", function(){

  var get_contact_id = $(this).attr("data-target");
  var type = "contact";

  swal.fire({
    type: "warning",
    title: "Remove Contact Detail",
    html: "<b>Are you sure you want to remove this contact detail?</b>",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/remove-detail-agent",
        data: {get_contact_id: get_contact_id, type: type},
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              successRemove();
              $(".contact_"+get_contact_id).remove();
            }
          }, 1500);
        },
      });
    }
  });

});





















































//

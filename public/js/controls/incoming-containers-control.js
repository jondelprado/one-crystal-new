//ADD CONTAINER BUTTON
var count = 1;

$(".add_container").on("click", function(){

  count++;
  var item_list = "";

  $.each(items, function(){
    item_list += "<option value='"+this.id+"'>"+this.item_name+"</option>";
  });

  $(".default_container").after('<div data-target="container_container" id="container_'+count+'" class="row p-2">'+
  '<div class="col-lg-6">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend">'+
  '<div class="input-group-text w-120"> <small>Container Size</small> </div>'+
  '</div>'+
  '<select class="form-control container_'+count+'_size">'+
  '<option value="">--Select Size--</option>'+
  '<option value="20">20</option>'+
  '<option value="40">40</option>'+
  '<option value="45">45</option>'+
  '<option value="LC">LC</option>'+
  '</select>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '<div class="col-lg-6">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend">'+
  '<div class="input-group-text w-150"> <small>Description of Goods</small> </div>'+
  '</div>'+
  '<select class="form-control container_'+count+'_item">'+
  '<option value="">--Select Item--</option>'+item_list+'</select>'+
  '<div class="input-group-append">'+
  '<button data-target="container_'+count+'" type="button" class="btn btn-danger remove_container">'+
  '<i class="fas fa-times"></i>'+
  '</button>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '<div class="col-lg-4">'+
  '<input type="text" class="form-control container_'+count+'_no" placeholder="Container No.">'+
  '</div>'+
  '<div class="col-lg-4">'+
  '<input type="number" class="form-control container_'+count+'_package" placeholder="No. of Packages">'+
  '</div>'+
  '<div class="col-lg-4">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<input type="number" class="form-control container_'+count+'_gross" placeholder="Gross Weight Cargo">'+
  '<div class="input-group-append">'+
  '<div class="input-group-text"> <small>Kg</small> </div>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>');

});

//CHECK BL/ACFTA VALUES
$(".btn_multi_proceed").on("click", function(){

  var empty_count = 0;
  var get_type = $(this).attr("data-target");
  var request_type = $(this).attr("id");

  $("input[name='select_ic[]']:checked").each(function(){
    var ic_id = $(this).val();
    if ($(".bl_container_"+ic_id).val() == "" ||
        $(".pl_container_"+ic_id).val() == "" ||
        $(".acfta_container_"+ic_id).val() == "") {
      empty_count++;
    }
  });

  if (empty_count == 0) {
    multipleProceed(get_type, request_type);
  }
  else {
    emptyFields();
  }

});

$(document).on("click", ".remove_container", function(){
  var get_id = $(this).attr("data-target");
  $("#"+get_id).remove();
});





















//

//CHECK SL PAYMENT/ FINAL DUTIES/ DEBIT DUTIES/ GATE PASS
$(".btn_multi_proceed").on("click", function(){

  var empty_count = 0;
  var get_type = $(this).attr("data-target");
  var request_type = $(this).attr("id");

  $("input[name='select_op[]']:checked").each(function(){
    var ic_id = $(this).val();
    if ($(".sl_container_"+ic_id).val() == "" ||
        $(".fd_container_"+ic_id).val() == "" ||
        $(".dd_container_"+ic_id).val() == "" ||
        $(".gp_container_"+ic_id).val() == "") {
      empty_count++;
    }
  });

  if (empty_count == 0) {
    multipleProceed(get_type, request_type);
  }
  else {
    emptyFields();
  }

});

//REVERT RECORDS
$(".btn_multi_revert").on("click", function(){

  var get_type = $(this).attr("data-target");
  var request_type = $(this).attr("id");

  multipleRevert(get_type, request_type);

});

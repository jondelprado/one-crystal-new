$(document).ready( function () {

  $('.select2').select2({
    tags: true,
    dropdownParent: $("#add_port_modal"),
    placeholder: "--Search--",
    allowClear: true,
    containerCssClass : "required_fields",
  });

});


//REMOVE BORDER COLOR ONCHANGE ONKEYPRESS ETC
$(document).on("change keypress", ".required_fields", function(){
  $(this).css("border-color", "");
});


//MULTIPLE RETIRE
$(".multi_retire").on("click", function(){
  if ($('.multi_retire:checked').length > 0) {
    $(".btn_multi_retire").fadeIn();
  }
  else {
    $(".btn_multi_retire").fadeOut();
  }
});

@extends('layouts.app')

@section('content')

  <div style="background: white;" class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h1>Customs Brokerage</h1><br>
            <button type="button" class="float_left mr-4 btn btn-primary" data-toggle="modal" data-target="#add_cb_modal"> <i class="fas fa-plus"></i> Add Customs Brokerage</button>

            <button type="button" id="cb_request" data-target="select_cb[]"
            class="btn hide float_left
            {{($status == "Active") ? "btn-danger" : "btn-warning"}}
            {{($status == "Active") ? "btn_multi_retire" : "btn_multi_restore"}}">
            <i class="fas fa-times"></i>
             {{($status == "Active") ? "Retire" : "Restore"}}
           </button>

            <select id="cb_filter" class="float_right status_filter">
              <option value="Active" {{($status == "Active") ? "selected" : ""}}>Active</option>
              <option value="Retired" {{($status == "Retired") ? "selected" : ""}}>Retired</option>
            </select>
          </div>

          <div class="card-body">
            <table class="table table-bordered table-hover table-striped profile_table">
              <thead>
                <tr>
                  <th class="text-center">
                    <input type="checkbox" class="check_all">
                  </th>
                  <th>Name</th>
                  <th>City</th>
                  <th>Contact Person</th>
                  <th>Position</th>
                  <th>Mobile Number</th>
                  <th>Email Address</th>
                  <th>Terms</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_customs_brokerage as $customs_brokerage)
                  <tr>
                    <td class="text-center">
                      <input data-target="{{ucwords($customs_brokerage->cb_name)}}" type="checkbox" class="multi_container" name="select_cb[]" value="{{$customs_brokerage->id}}">
                    </td>
                    <td>{{ucwords($customs_brokerage->cb_name)}}</td>
                    <td>
                      @foreach ($customs_brokerage->address as $cb_address)
                        {{ucwords($cb_address->cb_city)}}
                        @break
                      @endforeach
                    </td>
                    @foreach ($customs_brokerage->contact as $cb_cp)
                      <td>{{ucwords($cb_cp->cb_cp_name)}}</td>
                      <td>{{ucwords($cb_cp->cb_cp_position)}}</td>
                      <td>
                        @foreach ($cb_cp->detail as $cp_detail)
                          @if ($cp_detail->cb_cp_contact_type == "Mobile")
                            {{$cp_detail->cb_cp_contact}}
                            @break
                          @endif
                        @endforeach
                      </td>
                      <td>
                        @foreach ($cb_cp->detail as $cp_detail)
                          @if ($cp_detail->cb_cp_contact_type == "Email")
                            {{$cp_detail->cb_cp_contact}}
                            @break
                          @endif
                        @endforeach
                      </td>
                      @break
                    @endforeach
                    <td>{{$customs_brokerage->cb_terms}}</td>
                    <td>
                      <button id="{{$customs_brokerage->id}}" type="button" class="btn btn-info {{($status == "Active") ? "" : "hide"}}" data-toggle="modal" data-target="#edit_cb_modal_{{$customs_brokerage->id}}"> <i class="fas fa-edit"></i> Edit</button>
                      <button id="{{$customs_brokerage->id}}" type="button" data-type="cb-{{($status == "Active") ? "Retired" : "Active"}}"
                        data-target="{{strtoupper($customs_brokerage->cb_name)}}"
                        class="btn
                        {{($status == "Active") ? "btn-danger" : "btn-warning"}}
                        {{($status == "Active") ? "btn_retire" : "btn_restore"}}">
                        <i class="fas fa-times"></i> {{($status == "Active") ? "Retire" : "Restore"}}
                      </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="add_cb_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header occolor_bg text-white">
          <h5 class="modal-title">Add Customs Brokerage</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Name</div>
                  </div>
                  <input type="text" class="form-control required_fields" name="cb_name" placeholder="Full Name">
                </div>
              </div>
            </div>

            <div data-target="address_container" id="address_1" class="col-lg-12 p-1 default_address">
              <div class="col-lg-12">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-130">
                      <select data-target="address_field_1" class="form-control required_fields address_1_type">
                        <option value="Office" selected>Office</option>
                        <option value="Warehouse">Warehouse</option>
                        <option value="Home">Home</option>
                      </select>
                    </div>
                    <input data-target="address_field_1" type="text" class="form-control required_fields address_1_address" placeholder="Address">
                    <div class="input-group-append">
                      <button data-target="add_modal" type="button" class="btn btn-primary add_address">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">City</div>
                    </div>
                    <input data-target="address_field_1" type="text" class="form-control required_fields address_1_city" placeholder="City">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-4"></div>

            <div data-target="person_container" id="person_1" class="row p-2 default_person">
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Contact Person</div>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_name" placeholder="Full Name">
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Position</div>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_position" placeholder="Position">
                    <div class="input-group-append">
                      <button data-target="add_modal" type="button" class="btn btn-primary add_contact_person">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div data-target="contact_container" id="person_1_contact_1" class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-120">
                      <select data-target="person_field_1" class="form-control required_fields person_1_contact_type_1">
                        <option value="/Mobile-" selected>Mobile</option>
                        <option value="/Landline-">Landline</option>
                        <option value="/Fax-">Fax</option>
                        <option value="/Email-">Email</option>
                        <option value="/WeChat-">WeChat</option>
                      </select>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_contact_1" placeholder="Contact Details">
                  </div>
                </div>
              </div>

              <div data-target="contact_container" id="person_1_contact_2" class="col-lg-6 default_contact">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-120">
                      <select data-target="person_field_1" class="form-control required_fields person_1_contact_type_2">
                        <option value="/Mobile-">Mobile</option>
                        <option value="/Landline-">Landline</option>
                        <option value="/Fax-">Fax</option>
                        <option value="/Email-" selected>Email</option>
                        <option value="/WeChat-">WeChat</option>
                      </select>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_contact_2" placeholder="Contact Details">
                    <div class="input-group-append">
                      <button name="person_1" data-target="add_modal" type="button" class="btn btn-primary add_contact">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-4"></div>

            <div class="col-lg-12">
              <div class="form-group half">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Terms</div>
                  </div>
                  <input type="number" class="form-control required_fields text-right" name="cb_terms">
                  <div class="input-group-append">
                    <div class="input-group-text"> <small>Days</small> </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Notes</div>
                  </div>
                  <textarea class="form-control required_fields" rows="3" name="cb_note" ></textarea>
                </div>
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn occolor_bg text-white btn_done">Done</button>
        </div>
      </div>
    </div>
  </div>

  @foreach ($get_cb_details as $cbd)
    <div class="modal fade" id="edit_cb_modal_{{$cbd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header occolor_bg text-white">
            <h5 class="modal-title">Edit Customs Brokerage</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">

              <div class="col-lg-12">
                <div class="card card-primary card-outline card-outline-tabs">
                  <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#main_content_{{$cbd->id}}" role="tab" aria-selected="true">Main Details</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#add_content_{{$cbd->id}}" role="tab" aria-selected="false">Additional Details</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#consignee_content_{{$cbd->id}}" role="tab" aria-selected="false">Consignee Companies</a>
                      </li>
                    </ul>
                  </div>

                  <div class="card-body">

                    <div class="tab-content" id="custom-tabs-four-tabContent">

                      <div class="tab-pane fade show active" id="main_content_{{$cbd->id}}" role="tabpanel">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Name</div>
                                </div>
                                <input data-target="cb_id" type="hidden" class="form-control edit_required_fields_{{$cbd->id}}" value="{{ucwords($cbd->id)}}">
                                <input data-target="cb_name" type="text" class="form-control edit_required_fields_{{$cbd->id}}"  placeholder="Full Name" value="{{ucwords($cbd->cb_name)}}">
                              </div>
                            </div>
                          </div>
                          @php
                            $add_count = 1;
                            $address_count = count($cbd->address);
                          @endphp
                          @foreach ($cbd->address as $cba)
                            <div data-target="edit_address_container_{{$cbd->id}}" id="edit_address_{{$add_count}}_{{$cbd->id}}" class="col-lg-12 p-1">
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <div class="input-group mb-2">
                                    <div class="input-group-prepend w-130">
                                      <select data-target="edit_address_field_{{$add_count}}" class="form-control edit_required_fields_{{$cbd->id}} edit_address_{{$add_count}}_type">
                                        <option value="Office" {{($cba->cb_address_type == "Office") ? "selected" : ""}}>Office</option>
                                        <option value="Warehouse" {{($cba->cb_address_type == "Warehouse") ? "selected" : ""}}>Warehouse</option>
                                        <option value="Home" {{($cba->cb_address_type == "Home") ? "selected" : ""}}>Home</option>
                                      </select>
                                    </div>
                                    <input data-target="edit_address_field_{{$add_count}}" type="hidden" class="edit_address_{{$add_count}}_id" value="{{$cba->id}}">
                                    <input data-target="edit_address_field_{{$add_count}}" type="text" class="form-control edit_required_fields_{{$cbd->id}} edit_address_{{$add_count}}_address" placeholder="Address" value="{{ucwords($cba->cb_address)}}">
                                    @if ($address_count > 1)
                                      <div class="input-group-append">
                                        <button data-target="{{$cba->id}}|{{ucwords($cba->cb_address)}}" type="button" class="btn btn-danger remove_current_address">
                                          <i class="fas fa-times"></i>
                                        </button>
                                      </div>
                                    @endif
                                  </div>
                                </div>
                              </div>

                              <div class="col-lg-6">
                                <div class="form-group">
                                  <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">City</div>
                                    </div>
                                    <input data-target="edit_address_field_{{$add_count}}" type="text" class="form-control edit_required_fields_{{$cbd->id}} edit_address_{{$add_count}}_city" placeholder="City" value="{{ucwords($cba->cb_city)}}">
                                  </div>
                                </div>
                              </div>
                            </div>
                            @php
                              $add_count++;
                            @endphp
                          @endforeach

                          <div class="col-lg-12 mb-4"></div>
                          @php
                            $cp_count = 1;
                            $person_count = count($cbd->contact);
                          @endphp
                          @foreach ($cbd->contact as $cbcp)
                            @php
                              $cpd_count = 1;
                            @endphp
                            <div data-target="edit_person_container_{{$cbd->id}}" id="edit_person_{{$cp_count}}_{{$cbd->id}}" class="row p-1">

                              <div class="col-lg-11">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text">Contact Person</div>
                                        </div>
                                        <input data-target="edit_person_field_{{$cp_count}}" type="hidden" class="edit_person_{{$cp_count}}_id" value="{{$cbcp->id}}">
                                        <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$cbd->id}} edit_person_{{$cp_count}}_name cb_name" placeholder="Full Name" value="{{ucwords($cbcp->cb_cp_name)}}">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text">Position</div>
                                        </div>
                                        <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$cbd->id}} edit_person_{{$cp_count}}_position cb_position" placeholder="Position" value="{{ucwords($cbcp->cb_cp_position)}}">
                                        @if ($person_count > 1)
                                          <div class="input-group-append">
                                            <button data-target="{{$cbcp->id}}|{{ucwords($cbcp->cb_cp_name)}}" type="button" class="btn btn-danger remove_current_person">
                                              <i class="fas fa-times"></i>
                                            </button>
                                          </div>
                                        @endif
                                      </div>
                                    </div>
                                  </div>

                                  @foreach ($cbcp->detail as $cpd)
                                    @php
                                      $contact_count = count($cbcp->detail);
                                    @endphp
                                    <div data-target="edit_contact_container_{{$cbd->id}}" id="edit_person_{{$cp_count}}_contact_{{$cpd_count}}_{{$cbd->id}}" class="col-lg-6">
                                      <div class="form-group">
                                        <div class="input-group mb-2">
                                          <div class="input-group-prepend w-120">
                                            <select data-target="edit_person_field_{{$cp_count}}" class="form-control edit_required_fields_{{$cbd->id}} edit_person_{{$cp_count}}_contact_type_{{$cpd_count}}">
                                              <option value="/Mobile-" {{($cpd->cb_cp_contact_type == "Mobile") ? "selected" : ""}}>Mobile</option>
                                              <option value="/Landline-" {{($cpd->cb_cp_contact_type == "Landline") ? "selected" : ""}}>Landline</option>
                                              <option value="/Fax-" {{($cpd->cb_cp_contact_type == "Fax") ? "selected" : ""}}>Fax</option>
                                              <option value="/Email-" {{($cpd->cb_cp_contact_type == "Email") ? "selected" : ""}}>Email</option>
                                              <option value="/WeChat-" {{($cpd->cb_cp_contact_type == "WeChat") ? "selected" : ""}}>WeChat</option>
                                            </select>
                                          </div>
                                          <input data-target="edit_person_field_{{$cp_count}}" type="hidden" class="edit_person_{{$cp_count}}_contact_{{$cpd_count}}_id" value="{{$cpd->id}}">
                                          <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$cbd->id}} edit_person_{{$cp_count}}_contact_{{$cpd_count}}" placeholder="Contact Details" value="{{$cpd->cb_cp_contact}}">
                                          @if ($contact_count > 2)
                                            <div class="input-group-append">
                                              <button data-target="{{$cpd->id}}|{{$cpd->cb_cp_contact_type}}-{{ucwords($cpd->cb_cp_contact)}}" type="button" class="btn btn-danger remove_current_contact">
                                                <i class="fas fa-times"></i>
                                              </button>
                                            </div>
                                          @endif
                                        </div>
                                      </div>
                                    </div>
                                    @php
                                      $cpd_count++;
                                    @endphp
                                  @endforeach
                                </div>
                              </div>

                              <div class="col-lg-1">
                                @if ($contact_count <= 3)
                                  <button type="button" class="btn btn-primary add_current_contact"
                                    data-toggle="modal" data-target="#additional_detail_{{$cbcp->id}}">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                @endif
                              </div>

                              <div style="margin-top: 15%;" class="modal fade" id="additional_detail_{{$cbcp->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-xs" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header occolor_bg text-white">
                                      <h5 class="modal-title">Add Contact Detail - <b>{{ucwords($cbcp->cb_cp_name)}}</b> </h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <div class="input-group mb-2">
                                          <div class="input-group-prepend w-120">
                                            <select class="form-control acd_fields_{{$cbcp->id}} additional_contact_type_{{$cbcp->id}}">
                                              <option value="">--Select--</option>
                                              <option value="Mobile">Mobile</option>
                                              <option value="Landline">Landline</option>
                                              <option value="Fax">Fax</option>
                                              <option value="Email">Email</option>
                                              <option value="WeChat">WeChat</option>
                                            </select>
                                          </div>
                                          <input type="text" class="form-control acd_fields_{{$cbcp->id}} additional_contact_detail_{{$cbcp->id}}" placeholder="Contact Details">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button id="{{$cbcp->id}}" data-request="cb" type="button" class="btn btn-primary center add_contact_detail">Add</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                            @php
                              $cp_count++;
                            @endphp
                          @endforeach

                          <div class="col-lg-12 mb-4"></div>

                          <div class="col-lg-12">
                            <div class="form-group half">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Terms</div>
                                </div>
                                <input data-target="cb_terms" type="number" class="form-control edit_required_fields_{{$cbd->id}} text-right" value="{{$cbd->cb_terms}}">
                                <div class="input-group-append">
                                  <div class="input-group-text"> <small>Days</small> </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Notes</div>
                                </div>
                                <textarea data-target="cb_note" class="form-control edit_required_fields_{{$cbd->id}}" rows="3">{{$cbd->cb_note}}</textarea>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12 text-right">
                            <hr>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button data-target="{{$cbd->id}}" id="main_details" type="button" class="btn occolor_bg text-white btn_save">Save</button>
                          </div>

                        </div>
                      </div>

                      <div class="tab-pane fade show" id="add_content_{{$cbd->id}}" role="tabpanel">

                        <input data-target="cb_id" type="hidden" class="additional_fields_{{$cbd->id}}" value="{{ucwords($cbd->id)}}">

                        <div data-target="add_address_container_{{$cbd->id}}" id="add_address_1_{{$cbd->id}}" class="row p-1 add_default_address_{{$cbd->id}}">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-130">
                                  <select data-target="add_address_field_1" class="form-control additional_fields_{{$cbd->id}} add_address_1_type">
                                    <option value="">--Select--</option>
                                    <option value="Office">Office</option>
                                    <option value="Warehouse">Warehouse</option>
                                    <option value="Home">Home</option>
                                  </select>
                                </div>
                                <input data-target="add_address_field_1" type="text" class="form-control additional_fields_{{$cbd->id}} add_address_1_address" placeholder="Address">
                                <div class="input-group-append">
                                  <button data-target="{{$cbd->id}}" type="button" class="btn btn-primary additional_address">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">City</div>
                                </div>
                                <input data-target="add_address_field_1" type="text" class="form-control additional_fields_{{$cbd->id}} add_address_1_city" placeholder="City">
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 mb-4"></div>

                        <div data-target="add_person_container_{{$cbd->id}}" id="add_person_1_{{$cbd->id}}" class="row p-1 add_default_person_{{$cbd->id}}">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Contact Person</div>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$cbd->id}} add_person_1_name" placeholder="Full Name">
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Position</div>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$cbd->id}} add_person_1_position" placeholder="Position">
                                <div class="input-group-append">
                                  <button data-target="{{$cbd->id}}" type="button" class="btn btn-primary additional_person">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div data-target="add_contact_container_{{$cbd->id}}" id="add_person_1_contact_1_{{$cbd->id}}" class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-120">
                                  <select data-target="add_person_field_1" class="form-control additional_fields_{{$cbd->id}} add_person_1_contact_type_1">
                                    <option value="/Mobile-" selected>Mobile</option>
                                    <option value="/Landline-">Landline</option>
                                    <option value="/Fax-">Fax</option>
                                    <option value="/Email-">Email</option>
                                    <option value="/WeChat-">WeChat</option>
                                  </select>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$cbd->id}} add_person_1_contact_1" placeholder="Contact Details">
                              </div>
                            </div>
                          </div>

                          <div data-target="add_contact_container_{{$cbd->id}}" id="add_person_1_contact_2_{{$cbd->id}}" class="col-lg-6 add_default_contact_{{$cbd->id}}">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-120">
                                  <select data-target="add_person_field_1" class="form-control additional_fields_{{$cbd->id}} add_person_1_contact_type_2">
                                    <option value="/Mobile-">Mobile</option>
                                    <option value="/Landline-">Landline</option>
                                    <option value="/Fax-">Fax</option>
                                    <option value="/Email-" selected>Email</option>
                                    <option value="/WeChat-">WeChat</option>
                                  </select>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$cbd->id}} add_person_1_contact_2" placeholder="Contact Details">
                                <div class="input-group-append">
                                  <button name="add_person_1" data-target="{{$cbd->id}}" type="button" class="btn btn-primary additional_contact">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 text-right">
                          <hr>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button data-target="{{$cbd->id}}" id="add_details" type="button" class="btn occolor_bg text-white btn_save">Add</button>
                        </div>

                      </div>

                      <div class="tab-pane fade show" id="consignee_content_{{$cbd->id}}" role="tabpanel">

                        <div data-target="consignee_container_{{$cbd->id}}" id="consignee_1_{{$cbd->id}}" class="row default_consignee_{{$cbd->id}}">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Consignee Company</div>
                                </div>
                                <input class="form-control consignee_1_name consignee_fields_{{$cbd->id}}" type="text">
                                <div class="input-group-append">
                                  <button data-target="{{$cbd->id}}" type="button" class="{{$cbd->consignee->count()}} btn btn-primary add_consignee">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Address</div>
                                </div>
                                <input class="form-control consignee_1_address consignee_fields_{{$cbd->id}}" type="text">
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">SEC/DTI Reg.</div>
                                </div>
                                <input class="form-control consignee_1_registration consignee_fields_{{$cbd->id}}" type="text">
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">TIN</div>
                                </div>
                                <input class="form-control consignee_1_tin consignee_fields_{{$cbd->id}}" type="text">
                              </div>
                            </div>
                          </div>

                        </div>

                        @php
                          $cc_count = 1;
                        @endphp
                        @foreach ($cbd->consignee as $cb_consignee)
                          @php
                            $cc_count++;
                          @endphp
                          <div data-target="consignee_container_{{$cbd->id}}" id="consignee_{{$cc_count}}_{{$cbd->id}}" class="row">
                            <div class="col-lg-12">
                              <div class="form-group">
                                <div class="input-group mb-2">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text">Consignee Company</div>
                                  </div>
                                  <input class="form-control consignee_{{$cc_count}}_name consignee_fields_{{$cbd->id}}" type="text" value="{{ucwords($cb_consignee->cc_name)}}">
                                  <div class="input-group-append">
                                    <button data-target="{{$cb_consignee->id}}|{{ucwords($cb_consignee->cc_name)}}" type="button" class="btn btn-danger remove_current_consignee">
                                      <i class="fas fa-times"></i>
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="col-lg-12">
                              <div class="form-group">
                                <div class="input-group mb-2">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text">Address</div>
                                  </div>
                                  <input class="form-control consignee_{{$cc_count}}_address consignee_fields_{{$cbd->id}}" type="text" value="{{$cb_consignee->cc_address}}">
                                </div>
                              </div>
                            </div>

                            <div class="col-lg-6">
                              <div class="form-group">
                                <div class="input-group mb-2">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text">SEC/DTI Reg.</div>
                                  </div>
                                  <input class="form-control consignee_{{$cc_count}}_registration consignee_fields_{{$cbd->id}}" type="text" value="{{$cb_consignee->cc_registration}}">
                                </div>
                              </div>
                            </div>

                            <div class="col-lg-6">
                              <div class="form-group">
                                <div class="input-group mb-2">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text">TIN</div>
                                  </div>
                                  <input class="form-control consignee_{{$cc_count}}_tin consignee_fields_{{$cbd->id}}" type="text" value="{{$cb_consignee->cc_tin}}">
                                </div>
                              </div>
                            </div>

                          </div>
                        @endforeach

                        <div class="col-lg-12 text-right">
                          <hr>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button data-target="{{$cbd->id}}" id="consignee_details" type="button" class="btn occolor_bg text-white btn_save">Add</button>
                        </div>
                      </div>

                    </div>

                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  @endforeach

@endsection

@section('scripts')
  <script src="{{asset('js/controls/customs-brokerage-control.js')}}" charset="utf-8"></script>
  <script src="{{asset('js/crud/customs-brokerage-crud.js')}}" charset="utf-8"></script>
@endsection

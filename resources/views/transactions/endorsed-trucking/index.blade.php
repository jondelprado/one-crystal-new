@extends('layouts.app')

@section('content')

  <div style="background: white;" class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h1>Endorsed to Trucking</h1><br>
            <button type="button" id="to_delivered" data-target="select_et[]" class="btn btn-success btn_multi_proceed hide mr-1 float_left">
              <i class="fas fa-sign-in-alt"></i> Mark as Delivered
            </button>
            <select style="float: right;" name="">
              <option value="">Active</option>
              <option value="">Retired</option>
              <option value="">All</option>
            </select>
          </div>

          <div class="card-body">
            <table class="table table-bordered table-hover table-striped transactions_table">
              <thead class="occolor_header text-white">
                <tr>
                  <th class="text-center">
                    <input type="checkbox" class="check_all">
                  </th>
                  <th><small>Endorsed Date</small></th>
                  <th>
                    <small>Yard Date</small><br>
                    <small>CRO Validity</small><br>
                    <small>Storage Validity</small>
                  </th>
                  <th><small>Shipping Line</small></th>
                  <th><small>Port of Destination</small></th>
                  <th>
                    <small>Consignee Name</small><br>
                    <small>Container No.</small><br>
                    <small>Container Size</small>
                  </th>
                  <th>
                    <small>No. of Packages</small><br>
                    <small>Item(s)</small><br>
                    <small>Container Wt.</small>
                  </th>
                  <th>
                    <small>Trucking Company</small><br>
                    <small>PRO No.</small>
                  </th>
                  <th>
                    <small>Gate Pass</small><br>
                    <small>Load Date</small><br>
                    <small>Delivered Date</small>
                  </th>
                  <th>
                    <small>Empty Return <i class="fas fa-asterisk"></i></small><br>
                    <small>EIR <i class="fas fa-asterisk"></i></small>
                  </th>
                  <th>
                    <small>Remarks</small><br>
                    <small>Customer Name</small>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_endorsed_trucking as $ic)
                  <tr>
                    <td class="text-center">
                      <input data-target="{{$ic->id}}" type="checkbox" class="multi_container" name="select_et[]" value="{{$ic->id}}">
                    </td>
                    <td>
                      <span data-toggle="modal" data-target="#endorsed_date_modal_{{$ic->id}}" class="btn_endorsed_date clickable">
                        {{date("m-d-Y", strtotime($ic->ic_endorsed_date))}}
                      </span>
                    </td>
                    <td>
                      <span>
                        {{($ic->ic_yard_date != null) ? date("m-d-Y", strtotime($ic->ic_yard_date)) : "--"}}
                      </span><br>
                      <span class="occolor_td">
                        {{($ic->ic_cro_validity != null) ? date("m-d-Y", strtotime($ic->ic_cro_validity)) : "--"}}
                      </span><br>
                      <span>
                        {{($ic->ic_storage_validity != null) ? date("m-d-Y", strtotime($ic->ic_storage_validity)) : "--"}}
                      </span>
                    </td>
                    <td class="occolor_border">
                      <span>{{ucwords($ic->shipping->sl_name)}}</span>
                    </td>
                    <td>
                      <span>
                        {{ucwords($ic->discharge->port_country)}} - {{ucwords($ic->discharge->port_name)}}
                      </span>
                    </td>
                    <td class="occolor_border">
                      <span>
                        {{ucwords($ic->consignee->cc_name)}}
                      </span><br>
                      <span class="occolor_td">
                        {{$ic->ic_container_no}}
                      </span><br>
                      <span>
                        {{$ic->ic_container_size}}
                      </span>
                    </td>
                    <td>
                      <span>
                        {{number_format($ic->ic_packages,0,"",",")}}
                      </span><br>
                      <span class="occolor_td">
                        {{ucwords($ic->item->item_name)}}
                      </span><br>
                      <span>
                        {{number_format($ic->ic_gross_weight,0,"",",")}}
                      </span>
                    </td>
                    <td class="occolor_border">
                      <span data-toggle="modal" data-target="#trucking_comp_modal_{{$ic->id}}" class="btn_trucking_form clickable">
                        {{($ic->ic_trucking_company != null) ? ucwords($ic->trucking->tc_name) : "--"}}
                      </span><br>
                      <span class="occolor_td">
                        {{($ic->ic_pro_no != null) ? $ic->ic_pro_no : "--"}}
                      </span>
                    </td>
                    <td>
                      <span data-toggle="modal" data-target="#gp_form_modal_{{$ic->id}}" class="btn_gp_form clickable">
                        {{($ic->ic_gate_date != null) ? date("m-d-Y", strtotime($ic->ic_gate_date)) : "--"}}
                      </span><br>
                      <span data-toggle="modal" data-target="#load_date_modal_{{$ic->id}}" class="btn_load_date occolor_td clickable">
                        {{($ic->ic_load_date != null) ? date("m-d-Y", strtotime($ic->ic_load_date)) : "--"}}
                      </span><br>
                      <span data-toggle="modal" data-target="#delivered_date_modal_{{$ic->id}}" class="btn_delivered_date clickable">
                        {{($ic->ic_delivery_date != null) ? date("m-d-Y", strtotime($ic->ic_delivery_date)): "--"}}
                      </span>
                    </td>
                    <td>
                      <input type="hidden" class="empty_container_{{$ic->id}}" value="{{$ic->ic_empty_date}}">
                      <span data-toggle="modal" data-target="#empty_date_modal_{{$ic->id}}" class="btn_empty_date clickable">
                        {{($ic->ic_empty_date != null) ? date("m-d-Y", strtotime($ic->ic_empty_date)): "--"}}
                      </span><br>
                      <input type="hidden" class="eir_container_{{$ic->id}}" value="{{$ic->ic_eir_date}}">
                      <span data-toggle="modal" data-target="#eir_form_modal_{{$ic->id}}" class="occolor_td btn_eir_form clickable">
                        {{($ic->ic_eir_date != null) ? date("m-d-Y", strtotime($ic->ic_eir_date)): "--"}}
                      </span>
                    </td>
                    <td class="occolor_border">
                      <span data-toggle="modal" data-target="#remarks_rm_modal_{{$ic->id}}" class="btn_remarks_form clickable">
                        {{($ic->ic_final_remarks != null) ? $ic->ic_final_remarks : "--"}}
                      </span><br>
                      <span class="dark_td">{{ucwords($ic->customer->lc_name)}}</span>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  @foreach ($get_endorsed_trucking as $icd)
    @php
      $modal_arr = array("endorsed", "load", "delivered", "empty");
      $modal_arr2 = array("gp", "eir");
    @endphp

    @foreach ($modal_arr as $modal)
      @php
        $modal_title = "";
        $dateval = "";
        switch ($modal) {
          case 'endorsed':
            $modal_title .= "Endorsed";
            $dateval = $icd->ic_endorsed_date;
            break;

          case 'load':
            $modal_title .= "Load";
            $dateval = $icd->ic_load_date;
            break;

          case 'delivered':
            $modal_title .= "Delivered";
            $dateval = $icd->ic_delivery_date;
            break;

          default:
            $modal_title .= "Empty Return";
            $dateval = $icd->ic_empty_date;
            break;
        }
      @endphp
      <div class="modal fade" id="{{$modal}}_date_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header occolor_bg2">
              <h5 class="modal-title">Update {{$modal_title}} Date</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">Date</div>
                      </div>
                      <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                      <input type="date" class="form-control {{$modal}}_date_{{$icd->id}}" name="{{$modal}}_date" value="{{$dateval}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button data-target="{{$icd->id}}" id="{{$modal}}" type="button" class="btn occolor_bg text-white btn_date_done">Done</button>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    @foreach ($modal_arr2 as $modal2)
      @php
        $modal_title = "";
        $dateval = "";
        $file = "";
        switch ($modal2) {
          case 'gp':
            $modal_title .= "Gate Pass";
            $dateval = $icd->ic_gate_date;
            $file = $icd->ic_gate_pass_file;
            break;

          default:
            $modal_title .= "EIR";
            $dateval = ($icd->ic_eir_date != null) ? $icd->ic_eir_date : $current_date;
            $file = $icd->ic_eir_file;
            break;
        }
      @endphp
      <div class="modal fade" id="{{$modal2}}_form_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header occolor_bg2">
              <h5 class="modal-title">{{$modal_title}} Form</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">

                <div class="col-lg-12">
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">Date {{($modal2 == "eir") ? "Received" : ""}}</div>
                      </div>
                      <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                      <input type="date" class="form-control required_fields {{$modal2}}_date_{{$icd->id}}"
                      value="{{$dateval}}" {{($modal2 == "gp") ? "disabled" : ""}}>
                    </div>
                  </div>
                </div>

                <div class="col-lg-12">
                  <input type="file" class="{{($modal2 == "gp") ? "hide" : ""}} required_fields {{$modal2}}_file_{{$icd->id}}" name="{{$modal2}}_file"><br><br>
                  @if ($file != null)
                    @php
                      $explode_url = explode("/", $file);
                      $filename = $explode_url[2];
                    @endphp
                    <a class="{{$modal2}}_anchor_{{$icd->id}}" href="{{asset($file)}}" download>{{$filename}}</a>
                  @endif
                </div>

              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button data-target="{{$icd->id}}" type="button" class="{{($modal2 == "gp") ? "hide" : ""}} btn occolor_bg text-white btn_{{$modal2}}_done">Done</button>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    <div class="modal fade" id="trucking_comp_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header occolor_bg2">
            <h5 class="modal-title">Trucking Company</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">

              <div class="col-lg-12">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        List
                      </div>
                    </div>
                    <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                    <select class="form-control required_fields trucking_name_{{$icd->id}}">
                      <option value="">--Select--</option>
                      @foreach ($get_trucking_companies as $trucking_company)
                        <option value="{{$trucking_company->id}}" {{($icd->ic_trucking_company == $trucking_company->id) ? "selected" : ""}}>
                          {{ucwords($trucking_company->tc_name)}}
                        </option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button data-target="{{$icd->id}}" type="button" class="btn occolor_bg text-white btn_trucking_done">Done</button>
          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="remarks_rm_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header occolor_bg2">
            <h5 class="modal-title">Remarks</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                <textarea class="form-control required_fields remarks_{{$icd->id}}" rows="4">{{$icd->ic_final_remarks}}</textarea>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button data-target="{{$icd->id}}" type="button" class="btn occolor_bg text-white btn_remarks_done">Done</button>
          </div>
        </div>
      </div>
    </div>
  @endforeach

@endsection

@section('scripts')
  <script type="text/javascript">
    var asset_path = @json(asset("/"));
  </script>
  <script src="{{asset('js/controls/endorsed-trucking-control.js')}}" charset="utf-8"></script>
  <script src="{{asset('js/crud/endorsed-trucking-crud.js')}}" charset="utf-8"></script>
@endsection




















{{--  --}}

@extends('layouts.app')

@section('content')

  <div style="background: white;" class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h1>On Process</h1><br>
            <button type="button" id="to_trucking" data-target="select_op[]"
                    class="btn btn-success btn_multi_proceed hide mr-4 float_left">
              <i class="fas fa-sign-in-alt"></i> Endorse to Trucking
            </button>
            <button type="button" id="to_incoming" data-target="select_op[]"
                    class="btn btn-warning btn_multi_revert hide float_left text-white">
              <i class="fas fa-undo"></i> Revert
            </button>
            <select class="float_right" name="">
              <option value="">Active</option>
              <option value="">Retired</option>
              <option value="">All</option>
            </select>
          </div>

          <div class="card-body">
            <table class="table table-bordered table-hover table-striped transactions_table">
              <thead class="occolor_header text-white">
                <tr>
                  <th class="text-center">
                    <input type="checkbox" class="check_all">
                  </th>
                  <th><small>ETA</small></th>
                  <th><small>Yard Date</small></th>
                  <th>
                    <small>B/L</small><br>
                    <small>P/L</small><br>
                    <small>ACFTA Rec'd</small>
                  </th>
                  <th>
                    <small>Shipping Lines</small><br>
                    <small>B/L Number</small><br>
                  </th>
                  <th>
                    <small>Origin F. Time</small><br>
                    <small>Local F. Time</small><br>
                  </th>
                  <th>
                    <small>Port of Origin</small><br>
                    <small>Port of Destination</small><br>
                  </th>
                  <th>
                    <small>Consignee Name</small><br>
                    <small>Container No.</small><br>
                    <small>Container Size</small><br>
                  </th>
                  <th>
                    <small>No. of Packages</small><br>
                    <small>Item(s)</small><br>
                    <small>Container Wt.</small><br>
                  </th>
                  <th><small>Trucking <br> Included?</small></th>
                  <th>
                    <small>Reg. No.</small><br>
                    <small>Entry No.</small><br>
                    <small>PRO No.</small><br>
                  </th>
                  <th>
                    <small>S.Line Payment <i class="fas fa-asterisk"></i></small><br>
                    <small>CRO Validity</small><br>
                    <small>Storage Validity</small><br>
                  </th>
                  <th>
                    <small>Lodge Date</small><br>
                    <small>Final Date <i class="fas fa-asterisk"></i></small><br>
                    <small>Debit Date <i class="fas fa-asterisk"></i></small><br>
                  </th>
                  <th>
                    <small>Gate Pass Date <i class="fas fa-asterisk"></i></small><br>
                    <small>Customer Name</small><br>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_on_process as $ic)
                  <tr>
                    <td class="text-center">
                      <input data-target="{{$ic->id}}" type="checkbox" class="multi_container" name="select_op[]" value="{{$ic->id}}">
                    </td>
                    <td>
                      <span data-toggle="modal" data-target="#eta_date_modal_{{$ic->id}}" class="btn_eta_date clickable">
                        {{date("m-d-Y", strtotime($ic->ic_eta))}}
                      </span>
                    </td>
                    <td>
                      <span data-toggle="modal" data-target="#yard_date_modal_{{$ic->id}}" class="btn_yard_date clickable">
                        {{($ic->ic_yard_date != null) ? date("m-d-Y", strtotime($ic->ic_yard_date)) : "--"}}
                      </span>
                    </td>
                    <td class="occolor_border">
                      <span data-toggle="modal" data-target="#bl_modal_{{$ic->id}}" class="clickable">
                        {{($ic->ic_bl_date != null) ? date("m-d-Y", strtotime($ic->ic_bl_date)) : "--"}}
                      </span><br>
                      <span data-toggle="modal" data-target="#pl_modal_{{$ic->id}}" class="occolor_td clickable">
                        {{($ic->ic_pl_date != null) ? date("m-d-Y", strtotime($ic->ic_pl_date)) : "--"}}
                      </span><br>
                      <span data-toggle="modal" data-target="#acfta_modal_{{$ic->id}}" class="clickable">
                        {{($ic->ic_acfta_date != null) ? date("m-d-Y", strtotime($ic->ic_acfta_date)) : "--"}}
                      </span><br>
                    </td>
                    <td class="occolor_border">
                      <span>{{ucwords($ic->shipping->sl_name)}}</span><br>
                      <span class="occolor_td">{{$ic->ic_blno}}</span><br>
                    </td>
                    <td>
                      <span data-toggle="modal" data-target="#oft_time_modal_{{$ic->id}}" class="btn_oft clickable">
                        {{($ic->ic_oft != null) ? date("h:i a", strtotime($ic->ic_oft)) : "--"}}
                      </span><br>
                      <span data-toggle="modal" data-target="#lft_time_modal_{{$ic->id}}" class="occolor_td btn_lft clickable">
                        {{($ic->ic_lft != null) ? date("h:i a", strtotime($ic->ic_lft)) : "--"}}
                      </span><br>
                    </td>
                    <td>
                      <span>
                        {{ucwords($ic->loading->port_country)}} - {{ucwords($ic->loading->port_name)}}
                      </span><br>
                      <span class="occolor_td">
                        {{ucwords($ic->discharge->port_country)}} - {{ucwords($ic->discharge->port_name)}}
                      </span><br>
                    </td>
                    <td class="occolor_border">
                      <span>
                        {{ucwords($ic->consignee->cc_name)}}
                      </span><br>
                      <span class="occolor_td">
                        {{$ic->ic_container_no}}
                      </span><br>
                      <span>
                        {{$ic->ic_container_size}}
                      </span><br>
                    </td>
                    <td>
                      <span>
                        {{number_format($ic->ic_packages, 0, "", ",")}}
                      </span><br>
                      <span class="occolor_td">
                        {{ucwords($ic->item->item_name)}}
                      </span><br>
                      <span>
                        {{number_format($ic->ic_gross_weight, 0, "", ",")}}
                      </span><br>
                    </td>
                    <td>{{($ic->ic_external_trucking != null) ? $ic->ic_external_trucking : "No"}}</td>
                    <td class="occolor_border">
                      <span data-toggle="modal" data-target="#reg_no_modal_{{$ic->id}}" class="btn_reg clickable">
                        {{($ic->ic_reg_no != null) ? $ic->ic_reg_no : "--"}}
                      </span><br>
                      <span data-toggle="modal" data-target="#entry_no_modal_{{$ic->id}}" class="occolor_td btn_entry clickable">
                        {{($ic->ic_entry_no != null) ? $ic->ic_entry_no : "--"}}
                      </span><br>
                      <span data-toggle="modal" data-target="#pro_no_modal_{{$ic->id}}" class="btn_pro clickable">
                        {{($ic->ic_pro_no != null) ? $ic->ic_pro_no : "--"}}
                      </span><br>
                    </td>
                    <td class="occolor_border">

                      <input type="hidden" class="sl_container_{{$ic->id}}" value="{{$ic->ic_sl_payment_date}}">
                      <span data-toggle="modal" data-target="#sl_form_modal_{{$ic->id}}" class="btn_sl_form clickable">
                        {{($ic->ic_sl_payment_date != null) ? date("m-d-Y", strtotime($ic->ic_sl_payment_date)) : "--"}}
                      </span><br>

                      <span data-toggle="modal" data-target="#cro_date_modal_{{$ic->id}}" class="occolor_td btn_cro clickable">
                        {{($ic->ic_cro_validity != null) ? date("m-d-Y", strtotime($ic->ic_cro_validity)) : "--"}}
                      </span><br>

                      <span data-toggle="modal" data-target="#storage_date_modal_{{$ic->id}}" class="btn_storage clickable">
                        {{($ic->ic_storage_validity != null) ? date("m-d-Y", strtotime($ic->ic_storage_validity)) : "--"}}
                      </span>
                    </td>
                    <td>
                      <span data-toggle="modal" data-target="#lodge_date_modal_{{$ic->id}}" class="btn_lodge_date clickable">
                        {{($ic->ic_lodge_date != null) ? date("m-d-Y", strtotime($ic->ic_lodge_date)) : "--"}}
                      </span><br>

                      <input type="hidden" class="fd_container_{{$ic->id}}" value="{{$ic->ic_final_date}}">
                      <span data-toggle="modal" data-target="#fd_form_modal_{{$ic->id}}" class="occolor_td btn_fd_form clickable">
                        {{($ic->ic_final_date != null) ? date("m-d-Y", strtotime($ic->ic_final_date)) : "--"}}
                      </span><br>

                      <input type="hidden" class="dd_container_{{$ic->id}}" value="{{$ic->ic_debited_date}}">
                      <span data-toggle="modal" data-target="#dd_form_modal_{{$ic->id}}" class="btn_dd_form clickable">
                        {{($ic->ic_debited_date != null) ? date("m-d-Y", strtotime($ic->ic_debited_date)) : "--"}}
                      </span>
                    </td>
                    <td class="occolor_border">
                      <input type="hidden" class="gp_container_{{$ic->id}}" value="{{$ic->ic_gate_date}}">
                      <span data-toggle="modal" data-target="#gp_form_modal_{{$ic->id}}" class="btn_gp_form clickable">
                        {{($ic->ic_gate_date != null) ? date("m-d-Y", strtotime($ic->ic_gate_date)) : "--"}}
                      </span><br>
                      <span class="dark_td">{{ucwords($ic->customer->lc_name)}}</span>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  @foreach ($get_on_process as $icd)
    @php
      $modal_arr = array("eta", "yard", "lodge");
      $modal_arr2 = array("sl", "fd", "dd", "gp");
      $modal_arr3 = array("oft", "lft");
      $modal_arr4 = array("reg", "entry", "pro");
      $modal_arr5 = array("cro", "storage");
      $file_modal_arr = array("bl", "pl", "acfta");
    @endphp

    @foreach ($modal_arr as $modal)
      @php
        $modal_title = "";
        $dateval = "";
        switch ($modal) {
          case 'eta':
            $modal_title .= "ETA";
            $dateval .= $icd->ic_eta;
            break;

          case 'yard':
            $modal_title .= "Yard";
            $dateval .= $icd->ic_yard_date;
            break;

          default:
            $modal_title .= "Lodge";
            $dateval .= $icd->ic_lodge_date;
            break;
        }
      @endphp

      <div class="modal fade" id="{{$modal}}_date_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header occolor_bg2">
              <h5 class="modal-title">Update {{$modal_title}} Date</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text w-120">{{$modal_title}} {{($modal != "eta") ? "Date" : ""}}</div>
                      </div>
                      <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                      <input type="date" class="form-control required_fields {{$modal}}_date_{{$icd->id}}" name="{{$modal}}_date" value="{{$dateval}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button data-target="{{$icd->id}}" type="button" class="btn occolor_bg text-white btn_{{$modal}}_done">Done</button>
            </div>
          </div>
        </div>
      </div>

    @endforeach

    @foreach ($file_modal_arr as $file)
      @php
        switch ($file) {
          case 'bl':
            $upload_date = $icd->ic_bl_date;
            $filepath = $icd->ic_bl_file;
            break;

          case 'pl':
            $upload_date = $icd->ic_pl_date;
            $filepath = $icd->ic_pl_file;
            break;

          default:
            $upload_date = $icd->ic_acfta_date;
            $filepath = $icd->ic_acfta_file;
            break;
        }
      @endphp
      <div class="modal fade" id="{{$file}}_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">{{ucwords($file)}} File</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">Date Uploaded</div>
                      </div>
                      <input type="text" class="form-control" value="{{date("m-d-Y", strtotime($upload_date))}}" readonly>
                    </div>
                  </div><br>
                  @php
                    $explode_url = explode("/", $filepath);
                    $filename = $explode_url[2];
                  @endphp
                  <a href="{{asset($filepath)}}" download>{{$filename}}</a>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    @foreach ($modal_arr2 as $modal2)
      @php
        $modal_title = "";
        $file = "";
        $dateval = "";
        $duties = "";
        $sl_expenses = "";
        $sl_deposit = "";
        switch ($modal2) {
          case 'sl':
            $modal_title .= "Shipping Lines";
            $dateval .= $icd->ic_sl_payment_date;
            $sl_expenses .= $icd->ic_sl_expenses;
            $sl_deposit .= $icd->ic_sl_container_deposit;
            break;

          case 'fd':
            $modal_title .= "Final Duties";
            $dateval .= $icd->ic_final_date;
            $duties .= $icd->ic_final_duties;
            break;

          case 'dd':
            $modal_title .= "Debited Duties";
            $dateval .= $icd->ic_debited_date;
            $duties .= $icd->ic_debited_duties;
            break;

          default:
            $modal_title .= "Gate Pass";
            $file .= $icd->ic_gate_pass_file;
            $dateval .= $icd->ic_gate_date;
            break;
        }
      @endphp
      <div class="modal fade" id="{{$modal2}}_form_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header occolor_bg2">
              <h5 class="modal-title">{{$modal_title}} Form</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">

                <div class="col-lg-12">
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text w-170">Date</div>
                      </div>
                      <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                      <input type="date" class="form-control required_fields {{$modal2}}_date_{{$icd->id}}" name="{{$modal2}}_date" value="{{$dateval}}">
                    </div>
                  </div>
                </div>

                @if ($modal2 == "sl")
                  <div class="col-lg-12">
                    <div class="form-group">
                      <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text w-170"><small>Shipping Lines Expenses</small></div>
                        </div>
                        <input type="number" class="form-control required_fields {{$modal2}}_expenses_{{$icd->id}}" name="{{$modal2}}_expenses" value="{{$sl_expenses}}">
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-12">
                    <div class="form-group">
                      <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text w-170"><small>Container Deposit</small></div>
                        </div>
                        <input type="number" class="form-control required_fields {{$modal2}}_deposit_{{$icd->id}}" name="{{$modal2}}_deposit" value="{{$sl_deposit}}">
                      </div>
                    </div>
                  </div>
                @elseif ($modal2 == "fd" || $modal2 == "dd")
                  <div class="col-lg-12">
                    <div class="form-group">
                      <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text w-130">{{($modal2 == "fd") ? "Final" : "Debited"}} Duties</div>
                        </div>
                        <input type="number" class="form-control required_fields {{$modal2}}_duties_{{$icd->id}}" name="{{$modal2}}_duties" value="{{$duties}}">
                      </div>
                    </div>
                  </div>
                @else
                  <div class="col-lg-12">
                    <input type="file" class=" required_fields {{$modal2}}_file_{{$icd->id}}" name="{{$modal2}}_file"><br><br>
                    @if ($file != null)
                      @php
                        $explode_url = explode("/", $file);
                        $filename = $explode_url[2];
                      @endphp
                      <a class="{{$modal2}}_anchor_{{$icd->id}}" href="{{asset($file)}}" download>{{$filename}}</a>
                    @endif
                  </div>
                @endif

              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button data-target="{{$icd->id}}" type="button" class="btn occolor_bg text-white btn_{{$modal2}}_done">Done</button>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    @foreach ($modal_arr3 as $modal3)
      @php
        $modal_title = "";
        $timeval = "";
        switch ($modal3) {
          case 'oft':
            $modal_title .= "Origin F. Time";
            $timeval .= $icd->ic_oft;
            break;

          default:
            $modal_title .= "Local F. Time";
            $timeval .= $icd->ic_lft;
            break;
        }
      @endphp

      <div class="modal fade" id="{{$modal3}}_time_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header occolor_bg2">
              <h5 class="modal-title">Update {{$modal_title}}</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text w-120">Time</div>
                      </div>
                      <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                      <input type="time" class="form-control required_fields {{$modal3}}_time_{{$icd->id}}" value="{{$timeval}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button data-target="{{$icd->id}}" id="{{$modal3}}" type="button" class="btn occolor_bg text-white btn_time_done">Done</button>
            </div>
          </div>
        </div>
      </div>

    @endforeach

    @foreach ($modal_arr4 as $modal4)
      @php
        $modal_title = "";
        $noval = "";
        switch ($modal4) {
          case 'reg':
            $modal_title .= "Reg. No.";
            $noval .= $icd->ic_reg_no;
            break;

          case 'entry':
            $modal_title .= "Entry No.";
            $noval .= $icd->ic_entry_no;
            break;

          default:
            $modal_title .= "PRO No.";
            $noval .= $icd->ic_pro_no;
            break;
        }
      @endphp

      <div class="modal fade" id="{{$modal4}}_no_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header occolor_bg2">
              <h5 class="modal-title">Insert {{$modal_title}}</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text w-120">{{$modal_title}}</div>
                      </div>
                      <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                      <input type="text" class="form-control {{$modal4}}_no_{{$icd->id}}" value="{{$noval}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button data-target="{{$icd->id}}" id="{{$modal4}}" type="button" class="btn occolor_bg text-white btn_no_done">Done</button>
            </div>
          </div>
        </div>
      </div>

    @endforeach

    @foreach ($modal_arr5 as $modal5)
      @php
        $modal_title = "";
        $dateval = "";
        switch ($modal5) {
          case 'cro':
            $modal_title .= "CRO";
            $dateval = $icd->ic_cro_validity;
            break;

          default:
            $modal_title .= "Storage";
            $dateval = $icd->ic_storage_validity;
            break;
        }
      @endphp

      <div class="modal fade" id="{{$modal5}}_date_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header occolor_bg2">
              <h5 class="modal-title">Update {{$modal_title}} Validity</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text w-120">Date</div>
                      </div>
                      <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                      <input type="date" class="form-control {{$modal5}}_date_{{$icd->id}}" name="{{$modal5}}_date" value="{{$dateval}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button data-target="{{$icd->id}}" type="button" class="btn occolor_bg text-white btn_{{$modal5}}_done">Done</button>
            </div>
          </div>
        </div>
      </div>

    @endforeach

  @endforeach

@endsection


@section('scripts')
  <script type="text/javascript">
    var asset_path = @json(asset("/"));
  </script>
  <script src="{{asset('js/controls/on-process-control.js')}}" charset="utf-8"></script>
  <script src="{{asset('js/crud/on-process-crud.js')}}" charset="utf-8"></script>
@endsection








































{{--  --}}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignPartnersContactPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foreign_partners_contact_people', function (Blueprint $table) {
          $table->id();
          $table->string('fp_cp_name');
          $table->string('fp_cp_position');
          $table->unsignedBigInteger('fp_id');
          $table->foreign('fp_id')
          ->references('id')
          ->on('foreign_partners');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreign_partners_contact_people');
    }
}

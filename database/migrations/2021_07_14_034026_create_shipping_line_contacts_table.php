<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingLineContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_line_contacts', function (Blueprint $table) {
          $table->id();
          $table->string('sl_cp_contact_type');
          $table->string('sl_cp_contact');
          $table->unsignedBigInteger('sl_cp_id');
          $table->foreign('sl_cp_id')
          ->references('id')
          ->on('shipping_line_contact_people');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_line_contacts');
    }
}

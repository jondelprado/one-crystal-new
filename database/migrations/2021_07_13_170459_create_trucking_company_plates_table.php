<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTruckingCompanyPlatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucking_company_plates', function (Blueprint $table) {
          $table->id();
          $table->string('tc_plate');
          $table->unsignedBigInteger('tc_id');
          $table->foreign('tc_id')
          ->references('id')
          ->on('trucking_companies');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trucking_company_plates');
    }
}

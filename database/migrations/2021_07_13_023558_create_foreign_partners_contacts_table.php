<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignPartnersContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foreign_partners_contacts', function (Blueprint $table) {
          $table->id();
          $table->string('fp_cp_contact_type');
          $table->string('fp_cp_contact');
          $table->unsignedBigInteger('fp_cp_id');
          $table->foreign('fp_cp_id')
          ->references('id')
          ->on('foreign_partners_contact_people');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreign_partners_contacts');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTruckingCompanyContactPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucking_company_contact_people', function (Blueprint $table) {
          $table->id();
          $table->string('tc_cp_name');
          $table->string('tc_cp_position');
          $table->unsignedBigInteger('tc_id');
          $table->foreign('tc_id')
          ->references('id')
          ->on('trucking_companies');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trucking_company_contact_people');
    }
}

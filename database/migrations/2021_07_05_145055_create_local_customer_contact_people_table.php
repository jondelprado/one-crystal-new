<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalCustomerContactPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_customer_contact_people', function (Blueprint $table) {
          $table->id();
          $table->string('lc_cp_name');
          $table->string('lc_cp_position');
          $table->unsignedBigInteger('lc_id');
          $table->foreign('lc_id')
          ->references('id')
          ->on('local_customers');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_customer_contact_people');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTruckingCompanyContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucking_company_contacts', function (Blueprint $table) {
          $table->id();
          $table->string('tc_cp_contact_type');
          $table->string('tc_cp_contact');
          $table->unsignedBigInteger('tc_cp_id');
          $table->foreign('tc_cp_id')
          ->references('id')
          ->on('trucking_company_contact_people');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trucking_company_contacts');
    }
}
